import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faPlus,
  faFilter,
  faEye,
  faPenToSquare,
  faTrash,
  faFileExport
} from '@fortawesome/free-solid-svg-icons';
import { Form, Menu, theme, Button, Layout, Space, Table, Tag, Input, Modal, Select, notification} from 'antd';
const { Search } = Input;

const objectName = 'chi tiết sản phẩm'

const paginationConfig = {
  pageSize: 5, // Số lượng hàng trên mỗi trang
};
//cột
const columns = [
  {
    title: 'STT',
    dataIndex: 'id',
    key: 'id  ',
    width: '30px',
  },
  {
    title: 'Tên chi tiết sản phẩm',
    dataIndex: 'productStorageName',
    key: 'productStorageName',
    width:"300px"
    // alignTitle: 'center',
  },
  {
    title: 'Mô tả',
    dataIndex: 'description',
    key: 'description',
    // alignTitle: 'center',
  },
  {
    title: 'Màu sắc',
    dataIndex: 'color',
    key: 'color',
    width: "100px"

  },
  {
    title: 'Size',
    dataIndex: 'size',
    key: 'size',
    width: "50px"

  },
  {
    title: 'Số lượng',
    dataIndex: 'quantity',
    key: 'quantity',
    width: "100px"

  },
  {
    title: 'Giá nhập',
    key: 'importPrice',
    dataIndex: 'importPrice',
    width: "120px"
  },
  {
    title: 'Giá bán',
    dataIndex: 'exportPrice',
    key: 'exportPrice',
    width: "120px"

  },
  {
    title: 'Hành động',
    key: 'action',
    width: '95px',
    render: (_, record) => (
      <Space size="middle">
        <a style={{ color: "blue" }}><FontAwesomeIcon icon={faPenToSquare} onClick={() => { console.log("hi"); }} /></a>
        <a style={{ color: "red" }}><FontAwesomeIcon icon={faTrash} onClick={() => { console.log("hi"); }} /></a>
      </Space>
    ),
  },
];
const data_fake = [
  {
    "id": 1,
    "productStorageName": "Nike Air Force One [Đen 39]",
    "color": "Đen",
    "size": 39,
    "quantity": 50,
    "importPrice": 500000,
    "exportPrice": 800000
  },
  {
    "id": 2,
    "productStorageName": "Nike Air Force One [Trắng 38]",
    "color": "Trắng",
    "size": 38,
    "quantity": 30,
    "importPrice": 450000,
    "exportPrice": 750000
  },
  {
    "id": 3,
    "productStorageName": "Nike Air Force One [Xám 40]",
    "color": "Xám",
    "size": 40,
    "quantity": 40,
    "importPrice": 520000,
    "exportPrice": 850000
  },
  {
    "id": 4,
    "productStorageName": "Nike Air Force One [Xanh 41]",
    "color": "Xanh",
    "size": 41,
    "quantity": 20,
    "importPrice": 480000,
    "exportPrice": 780000
  },
  {
    "id": 5,
    "productStorageName": "Nike Air Force One [Đỏ 37]",
    "color": "Đỏ",
    "size": 37,
    "quantity": 25,
    "importPrice": 510000,
    "exportPrice": 830000
  },
  {
    "id": 6,
    "productStorageName": "Nike Air Force One [Đen 42]",
    "color": "Đen",
    "size": 42,
    "quantity": 35,
    "importPrice": 530000,
    "exportPrice": 870000
  },
  {
    "id": 7,
    "productStorageName": "Nike Air Force One [Trắng 36]",
    "color": "Trắng",
    "size": 36,
    "quantity": 15,
    "importPrice": 470000,
    "exportPrice": 770000
  },
  {
    "id": 8,
    "productStorageName": "Nike Air Force One [Xám 39]",
    "color": "Xám",
    "size": 39,
    "quantity": 22,
    "importPrice": 490000,
    "exportPrice": 800000
  },
  {
    "id": 9,
    "productStorageName": "Nike Air Force One [Xanh 38]",
    "color": "Xanh",
    "size": 38,
    "quantity": 28,
    "importPrice": 520000,
    "exportPrice": 850000
  },
  {
    "id": 10,
    "productStorageName": "Nike Air Force One [Đỏ 40]",
    "color": "Đỏ",
    "size": 40,
    "quantity": 33,
    "importPrice": 500000,
    "exportPrice": 820000
  },
  {
    "id": 11,
    "productStorageName": "Nike Air Force One [Đen 37]",
    "color": "Đen",
    "size": 37,
    "quantity": 45,
    "importPrice": 480000,
    "exportPrice": 790000
  }
]

  // tìm kiếm
  const onSearch = (value, _e, info) => console.log(info?.source, value);

export default function ProductDetailComponent({ isOpen, closeModal, onConfirm }) {

  // const [name, setName] = useState('');
  // const [category, setCategory] = useState('');
  // const [brand, setBrand] = useState('');
  // const [importPrice, setImportPrice] = useState('');
  // const [exportPrice, setExportPrice] = useState('');
  // const [description, setDescription] = useState('');
  // const [imgUrl, setUrlImg] = useState('');

  // const handleOk = (e) => {
  //     onConfirm(e)
  // };
  const handleCancel = () => {
    closeModal();
  };


  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };

  return (
    <Modal
      style={{
        marginLeft: "260px",
        marginRight: "20px",
        marginBottom: "-20px",
        marginTop: "-10px",
        // backgroundColor:"red",
      }}
      width={"100"}
      height={"100"}
      title={<h3 style={{
        marginTop: "-2px",
        marginBottom: "30px",
        fontWeight: "bold"
      }}> {objectName} : Nike Air Force One</h3>} open={isOpen} onCancel={handleCancel}
      onOk={() => handleOk({})} >
      {/* <div>
        <h3>Nike Air Force One</h3>
      </div> */}
 <Search placeholder={`Nhập tên ${objectName}`} allowClear enterButton="Search" size="large" onSearch={onSearch}
                style={{
                  width: "460px",
                  // marginBottom: "20px",
                }}
              />
      <Button type="primary" size='large'
        //  onClick={() => setModalAdd(true)}
        style={{
          backgroundColor: "green",
          display: "block",
          margin: "20px 0"
        }}
      >
        Thêm mới<FontAwesomeIcon icon={faPlus} style={{ marginLeft: "10px" }} />
      </Button>

      <div className=''
        style={{
          height: "100%",
          color: "#fff"
        }}>
        <Table columns={columns} dataSource={data_fake} pagination={paginationConfig} />
      </div>
      <Button type="primary" size='large'
        style={{
          backgroundColor: "orange",
          marginTop: "20px"
        }}>Xuất dữ liệu <FontAwesomeIcon icon={faFileExport} style={{ marginLeft: "10px", color: "#fff" }} onClick={''} />
      </Button>
    </Modal>
  )
}
