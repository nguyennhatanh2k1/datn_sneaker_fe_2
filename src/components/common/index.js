// import { useState } from 'react';
// import Pagination from '@material-ui/lab/Pagination';
// import SearchRoundedIcon from '@material-ui/icons/SearchRounded';
// import { makeStyles, createStyles } from '@material-ui/core/styles';
// import CategoryComponent from '../category/categoryComponent';
// import CategoryDetailComponent from '../category/CategoryDetailComponent'
// import QuestionComponent from '../question/questionComponent';
// import QuizDetailComponent from '../question/QuizDetailComponent';
// import AnswerComponent from '../anwser/answerComponent';
// import AnswerDetailComponent from '../anwser/AnswerDetailComponent'
// import CriteriaComponent from '../criteria/criteriaComponent';
// import CriteriaDetail from '../criteria/CriteriaDetail';
// import ResultComponent from '../result/resultComponent';
// import ResulDetailComponent from '../result/ResulDetailComponent';
// import ModalComponent from '../category/ModalComponent';
// import IconButton from '@material-ui/core/IconButton';
// import EditIcon from '@material-ui/icons/Edit';
// import Button from '@material-ui/core/Button';
// import TextField from '@material-ui/core/TextField';
// import Dialog from '@material-ui/core/Dialog';
// import DialogActions from '@material-ui/core/DialogActions';
// import DialogContent from '@material-ui/core/DialogContent';
// import CreateResult from './CreateResult'
// import '../../styles/main.css';


// const useStyles = makeStyles((theme) =>
//     createStyles({
//         paginationBtn: {
//             '& .Mui-selected': {
//                 backgroundColor: '#141532',
//                 color: '#fff'
//             }, '& .Mui-selected:hover': {
//                 backgroundColor: 'rgba(20, 21, 50,.9)'
//             },
//         },
//         btn: {
//             backgroundColor: "#fff",
//             padding: "5px 10px",
//             fontSize: '14px',
//             boxShadow: 'none',
//             borderRadius: '5px',
//             border: '1px solid #ccc',
//             textTransform: 'none',
//             color: "#141532",
//             "&:hover": {
//                 backgroundColor: "#fff",
//                 boxShadow: 'none',
//             }
//         },
//         modal: {
//             '& .MuiDialog-paperWidthSm': {
//                 witdh: '80vw'
//             }
//         }
//     })
// );


// export default function QuizComponent(props) {
//     const classes = useStyles();
//     const [data, setData] = useState({
//         id: '',
//         name: props.name,
//         question: [],
//         display: 'none',
//         keySearch: '',
//         open: false,
//         detailName: '',
//         openDialog: false,
//         displayDetail: 'none'

//     })
//     let date = `${new Date().getDate()}/${new Date().getMonth() + 1}/${new Date().getFullYear()}`;

//     const handleClickOpen = () => {
//         setData({
//             ...data,
//             detailName: props.categoryDetail.contentCategory,
//             openDialog: true
//         });
//     };

//     const handleClose = () => {
//         setData({
//             ...data,
//             openDialog: false
//         });
//     };
//     const handleUpdate = () => {
//         props.updateDispatch({ id: data.id, name: data.detailName, author: 'Nguyễn Văn A', date: date })
//         setData({
//             ...data,
//             openDialog: false
//         });

//     }
//     const handleAdd = () => {
//         props.addDispatch({ name: data.keySearch, author: 'Nguyễn Văn A', date: date })
//         setData({
//             ...data,
//             keySearch: ''
//         })

//     }
//     const handleSearch = () => {
//         props.searchDispatch({ keySearch: data.keySearch, activePage: 1 })
//     }
//     let showListData = [];
//     let detailData = [];
//     let title = '';
//     let addBtn = '';
//     let nameDetail = '';
//     let titleBtn = '';

//     console.log("common state: ", props);
//     if (props.listCategory) {
//         showListData = props.listCategory.map((value, key) => (
//             <CategoryComponent key={key}  {...props} item={value} stt={key}
//                 onClickItem={(data) => setData(data)} categoryData={data} />
//         ));
//         detailData = props.categoryDetail.question.map((item, keyquestion) => (
//             <CategoryDetailComponent key={keyquestion} {...props} item={item} categoryData={data} onClickItem={(data) => setData(data)} />
//         ));
//         title = "Danh mục tư vấn";
//         titleBtn = "Thêm danh mục";
//         nameDetail = (
//             <div className="question" style={{ background: "#fff", display: data.display }}>
//                 <h3>{props.categoryDetail.contentCategory}</h3>
//                 <IconButton aria-label="edit" onClick={handleClickOpen} style={{ display: data.display }}>
//                     <EditIcon />
//                 </IconButton>
//             </div>
//         )
//         addBtn = (<ModalComponent {...props} data={data} />);

//     } else if (props.listQuestion) {
//         showListData =
//             props.listQuestion.map((value, key) => (
//                 <QuestionComponent key={key}  {...props} item={value} stt={key}
//                     onClickItem={(data) => setData(data)} />
//             ));
//         detailData = (
//             <QuizDetailComponent {...props} display={data.display} />
//         )
//         title = "Câu hỏi";
//         titleBtn = "Thêm câu hỏi";
//         nameDetail = (
//             <div className="question" style={{ background: "#fff", display: data.display }}>
//                 <h3>{props.questionDetail.contentQuestion}</h3>
//                 <IconButton aria-label="edit" onClick={() => {
//                     setData({
//                         ...data,
//                         detailName: props.questionDetail.contentQuestion,
//                         openDialog: true
//                     });
//                 }} style={{ display: data.display }}>
//                     <EditIcon />
//                 </IconButton>
//             </div>
//         )
//     } else if (props.listAnswer) {
//         showListData =
//             props.listAnswer.map((value, key) => (
//                 <AnswerComponent key={key}  {...props} item={value} stt={key}
//                     onClickItem={(data) => setData(data)} />
//             ));
//         detailData = (
//             <AnswerDetailComponent {...props} display={data.display} />
//         )
//         title = "Câu trả lời";
//         titleBtn = "Thêm câu trả lời ";
//         nameDetail = (
//             <div className="question" style={{ background: "#fff", display: data.display }}>
//                 <h3>{props.answerDetail.contentAnswer}</h3>
//                 <IconButton aria-label="edit" onClick={() => {
//                     setData({
//                         ...data,
//                         detailName: props.answerDetail.contentAnswer,
//                         openDialog: true
//                     });
//                 }} style={{ display: data.display }}>
//                     <EditIcon />
//                 </IconButton>
//             </div>
//         )
//     } else if (props.listCriteria) {
//         showListData =
//             props.listCriteria.map((value, key) => (
//                 <CriteriaComponent key={key}  {...props} item={value} stt={key}
//                     onClickItem={(data) => setData(data)} />
//             ));
//         detailData = (
//             <CriteriaDetail {...props} display={data.display} />
//         )
//         title = "Tiêu chí";
//         titleBtn = "Thêm tiêu chí";
//         nameDetail = (
//             <div className="question" style={{ background: "#fff", display: data.display }}>
//                 <h3>{props.criteriaDetail.contentCriteria}</h3>
//                 <IconButton aria-label="edit" onClick={() => {
//                     setData({
//                         ...data,
//                         detailName: props.criteriaDetail.contentCriteria,
//                         openDialog: true
//                     });
//                 }} style={{ display: data.display }}>
//                     <EditIcon />
//                 </IconButton>
//             </div>
//         )
//     } else if (props.listResult) {
//         showListData =
//             props.listResult.map((value, key) => (
//                 <ResultComponent key={key}  {...props} item={value} stt={key}
//                     onClickItem={(data) => setData(data)} />
//             ));
//         detailData = (
//             <ResulDetailComponent {...props} display={data.display} />
//         )
//         title = "Kết quả tư vấn";
//         titleBtn = "Thêm kết quả";
//         nameDetail = (
//             <div className="question" style={{ background: "#fff", display: data.display }}>
//                 <h3>{props.resultDetail.contentResult}</h3>
//                 <CreateResult {...props} idResult={props.resultDetail._id} />
//             </div>
//         )
//     }
//     return (
//         <div className="main-screen">
//             <h1>{title} </h1>
//             <div className="container">
//                 <div className="container-left">
//                     <div className="container-content" >
//                         <div className="container-content-top">
//                             <div className="search">
//                                 <input type="text"
//                                     value={data.keySearch}
//                                     onChange={(e) => setData({
//                                         ...data,
//                                         keySearch: e.target.value
//                                     })}
//                                     onKeyPress={(e) => (e.key === "Enter") && handleSearch()}
//                                     required
//                                 />
//                                 <button onClick={handleSearch}>
//                                     <SearchRoundedIcon className="search-icon" />
//                                 </button>
//                             </div>
//                             {
//                                 props.listResult ?
//                                     <CreateResult {...props} idResult='' />
//                                     :
//                                     <button className="add-btn" onClick={handleAdd}>
//                                         {titleBtn}
//                                     </button>
//                             }

//                         </div>
//                         <div className="container-content-bottom" id='style-3'>
//                             <div className="table">
//                                 <div className="table-head">
//                                     <div className="table-stt">STT</div>
//                                     <div className="table-name">{title}</div>
//                                     <div className="table-author">Author</div>
//                                     <div className="table-date">Date</div>
//                                     <div className="table-action">Hành động</div>
//                                 </div>
//                                 {showListData}
//                             </div>
//                         </div>

//                     </div>
//                     {props.keySearch ?
//                         <Pagination shape="rounded" variant="outlined" count={props.totalPage} color="primary"
//                             className={classes.paginationBtn}
//                             hidePrevButton hideNextButton onChange={(event, value) => props.searchDispatch({ keySearch: props.keySearch, activePage: value })} />
//                         :
//                         <Pagination shape="rounded" variant="outlined" count={props.totalPage} color="primary"
//                             className={classes.paginationBtn}
//                             hidePrevButton hideNextButton onChange={(event, value) => props.getDisatch(value)} />
//                     }


//                 </div>
//                 <div className="container-right">
//                     <div className="container-content" id="scrollbar">
//                         {nameDetail}
//                         <div className="content-detail">
//                             {detailData}
//                         </div>
//                         {addBtn}
//                     </div>
//                 </div>





//             </div>

//             <Dialog open={data.openDialog} onClose={handleClose} fullWidth maxWidth="sm" aria-labelledby="form-dialog-title">
//                 <DialogContent className={classes.modal}>
//                     <TextField
//                         autoFocus
//                         margin="dense"
//                         value={data.detailName}
//                         label={title}
//                         fullWidth
//                         onChange={(e) => setData({
//                             ...data,
//                             detailName: e.target.value
//                         })}
//                         onKeyPress={(e) => (e.key === "Enter") && handleUpdate()}
//                     />
//                 </DialogContent>
//                 <DialogActions>
//                     <Button onClick={handleClose} color="primary">
//                         Cancel
//                     </Button>
//                     <Button onClick={handleUpdate} color="primary">
//                         Update
//                     </Button>
//                 </DialogActions>
//             </Dialog>

//         </div >

//     )
// }

import React, { useState } from 'react';
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  AudioOutlined
} from '@ant-design/icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBookmark, faClipboardList, faUser, faShoePrints, faPlus, faArrowRight, faFilter } from '@fortawesome/free-solid-svg-icons';
import { faProductHunt, faShopify } from '@fortawesome/free-brands-svg-icons';
import { Menu, theme, Button, Layout, Space, Table, Tag, Input, Modal, Select } from 'antd';
const { Header, Content, Footer } = Layout;
const { Search } = Input;

const suffix = (
  <AudioOutlined
    style={{
      fontSize: 16,
      color: '#1677ff',
    }}
  />
);
const onSearch = (value, _e, info) => console.log(info?.source, value);
const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    render: (text) => <a>{text}</a>,
  },
  {
    title: 'Age',
    dataIndex: 'age',
    key: 'age',
  },
  {
    title: 'Address',
    dataIndex: 'address',
    key: 'address',
  },
  {
    title: 'Tags',
    key: 'tags',
    dataIndex: 'tags',
    render: (_, { tags }) => (
      <>
        {tags.map((tag) => {
          let color = tag.length > 5 ? 'geekblue' : 'green';
          if (tag === 'loser') {
            color = 'volcano';
          }
          return (
            <Tag color={color} key={tag}>
              {tag.toUpperCase()}
            </Tag>
          );
        })}
      </>
    ),
  },
  {
    title: 'Action',
    key: 'action',
    render: (_, record) => (
      <Space size="middle">
        <a>Invite {record.name}</a>
        <a>Delete</a>
      </Space>
    ),
  },
];
const data = [
  {
    key: '1',
    name: 'John Brown',
    age: 32,
    address: 'New York No. 1 Lake Park',
    tags: ['nice', 'developer'],
  },
  {
    key: '2',
    name: 'Jim Green',
    age: 42,
    address: 'London No. 1 Lake Park',
    tags: ['loser'],
  },
  {
    key: '3',
    name: 'Joe Black',
    age: 32,
    address: 'Sydney No. 1 Lake Park',
    tags: ['cool', 'teacher'],
  },
];
const App = () => {
  const [isButtonVisible, setIsButtonVisible] = useState(false);

  const toggleButtonVisibility = () => {
    setIsButtonVisible(!isButtonVisible);
  };
  const [collapsed, setCollapsed] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  // const {
  //   token: { colorBgContainer },
  // } = theme.useToken();
  return (
    <Layout className="layout">
      <Header
        style={{
          padding: 0,
          // background: colorBgContainer,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          margin: "20px 0"
        }}
      >
        <Button
          type="text"
          icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
          onClick={() => setCollapsed(!collapsed)}
          style={{
            fontSize: '16px',
            width: 64,
            height: 64,
            color: "white"
          }}
        />
        <h1 style={{
          margin: "20px 0",
          color: "#fff"
        }}>Quản lý sản phẩm</h1>
        <h5 style={{
          margin: "20px 20px 20px 0",
          color: "#fff",
        }}><a>Đăng xuất</a></h5>
      </Header>

      <Content
        style={{
          padding: '0 50px',
        }}
      >
        <div
          className="site-layout-content"

        >
          <div className='search-filter'
            style={{
              display: "flex",
              justifyContent: "space-between",
              margin: "50px 0"
            }}>
            <div className='left'
              style={{
                width: "50%",
                border: "1px solid black"
              }}>
              <Search
                placeholder="Nhập tên công việc"
                allowClear
                enterButton="Search"
                size="large"
                onSearch={onSearch}
                style={{
                  width: "400px"
                }}
              />
              <Button type="primary" onClick={showModal}
                style={{
                  backgroundColor: "green",
                  display: "block",
                  margin: "20px 0"
                }}>
                Thêm sản phẩm<FontAwesomeIcon icon={faPlus} style={{ marginLeft: "10px" }} />
              </Button>
              <Modal title="Basic Modal" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                <p>Some contents...</p>
                <p>Some contents...</p>
                <p>Some contents...</p>
              </Modal>
            </div>
            <div className='right'
              style={{
                width: "50%",
                border: "1px solid black",
                // display:"flex",
                // flexDirection: "column",
                // alignItems:"end",
                // justifyContent:"end"
              }}
            >
              <label>
                <input
                  type="checkbox"
                  checked={isButtonVisible}
                  onChange={toggleButtonVisibility}
                  style={{
                    display: "none"
                  }}
                />
                <strong style={{
                  fontSize: "20px",
                  border: "red 2px solid"
                }}><FontAwesomeIcon icon={faFilter}
                  style={{
                    marginRight: "5px",
                  }} /> Lọc </strong>
              </label>

              {isButtonVisible && (
                <div>
                  <Select
                    defaultValue="Chọn danh mục"
                    style={{ width: 200 }}
                    // onChange={handleChange}                
                    options={[
                      { value: 'jack', label: 'Giày thể thao' },
                      { value: 'lucy', label: 'Giày thời trang' },
                      { value: 'Yiminghe', label: 'Giày bệt' },
                    ]}
                  />
                </div>
              )}
            </div>
          </div>


          <Table columns={columns} dataSource={data} />
          <Button type="primary"
            style={{
              backgroundColor: "orange"
            }}>Xuất dữ liệu <FontAwesomeIcon icon={faArrowRight} style={{ marginLeft: "10px" }} />
          </Button>
        </div>
      </Content>
      <Footer
        style={{
          textAlign: 'center',
        }}
      >
        Ant Design ©2023 Created by Ant UED
      </Footer>
    </Layout>
  );
};
export default App;
