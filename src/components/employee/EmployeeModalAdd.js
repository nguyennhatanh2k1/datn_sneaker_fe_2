import React, { useState } from 'react';
import { Form, Input, Modal, Col, Row, Select, InputNumber, Upload } from 'antd';
import ImgCrop from 'antd-img-crop';


const objectName = 'nhân viên'

// // dữ liệu modal ảo
// const category = [
//     { value: 'Giày thể thao', label: 'Giày thể thao' },
//     { value: 'Giày thời trang', label: 'Giày thời trang' },
//     { value: 'Giày bệt', label: 'Giày bệt' },
// ]

// const product_brand = [
//     { value: 'Nike', label: 'Nike' },
//     { value: 'Adidas', label: 'Adidas' },
//     { value: 'MLB', label: 'MLB' },
// ]
// const product_color = [
//     { value: 'Trắng', label: 'Trắng' },
//     { value: 'Đen', label: 'Đen' },
//     { value: 'Xanh lá', label: 'Xanh lá' },
//     { value: 'Đỏ', label: 'Đỏ' },
// ]
// const product_size = [
//     { value: '35', label: '35' },
//     { value: '36', label: '36' },
//     { value: '37', label: '37' },
//     { value: '38', label: '38' },
//     { value: '39', label: '39' },
//     { value: '40', label: '40' },
//     { value: '41', label: '41' },
//     { value: '42', label: '42' },
//     { value: '43', label: '43' },
// ]

export default function EmployeeModalAdd({ isOpen, closeModal, onConfirm }) {
    const { TextArea } = Input;

    // const [name, setName] = useState('');
    // const [category, setCategory] = useState('');
    // const [brand, setBrand] = useState('');
    // const [importPrice, setImportPrice] = useState('');
    // const [exportPrice, setExportPrice] = useState('');
    // const [description, setDescription] = useState('');
    // const [imgUrl, setUrlImg] = useState('');

    // const handleOk = (e) => {
    //     onConfirm(e)
    // };
    const handleCancel = () => {
        closeModal();
    };


    const [isModalOpen, setIsModalOpen] = useState(false);
    const showModal = () => {
        setIsModalOpen(true);
    };
    const handleOk = () => {
        setIsModalOpen(false);
    };

    // upload ảnh
    const [fileList, setFileList] = useState([]);
    const changeImage = ({ fileList: newFileList }) => {
        setFileList(newFileList);
    };
    const previewImage = async (file) => {
        let src = file.url;
        if (!src) {
            src = await new Promise((resolve) => {
                const reader = new FileReader();
                reader.readAsDataURL(file.originFileObj);
                reader.onload = () => resolve(reader.result);
            });
        }
        const image = new Image();
        image.src = src;
        const imgWindow = window.open(src);
        imgWindow?.document.write(image.outerHTML);
    };
    return (
        <Modal
            style={{
                marginLeft: "260px",
                marginRight: "20px",
                marginBottom: "-20px",
                marginTop: "-17px",
                // backgroundColor:"red",
            }}
            width={"100"}
            height={"100"}
            title={<h3 style={{
                marginTop: "-2px",
                marginBottom: "30px",
                fontWeight: "bold"
            }}>Thêm {objectName}</h3>} open={isOpen} onCancel={handleCancel}
            onOk={() => handleOk({})} >
            <Form
            // disabled={true}
            >
                <Row gutter={20}>
                    <Col span={15}
                        style={{
                            // padding:"0 20px",
                            // margin:"0 -20px"
                        }}>
                        {/* tên sp */}
                        <Row>
                            <Col span={24}
                                style={{
                                    // padding:"0 20px",
                                    // margin:"0 -20px"
                                }}>
                                <Form.Item
                                    label={<span style={{
                                        fontSize: "16px",
                                        fontWeight: "bold"
                                    }}>Tên {objectName}</span>}
                                    name="name"
                                    rules={[
                                        {
                                            required: true,
                                            message: `Vui lòng nhập tên ${objectName} !`,
                                        },
                                    ]}
                                    labelCol={{
                                        span: 24,
                                    }}
                                    wrapperCol={{
                                        span: 24,
                                    }}
                                    labelAlign='left'
                                    style={{
                                        display: 'flex',
                                        flexDirection: 'column',
                                    }}
                                >
                                    <Input allowClear size='large' placeholder={`Nhập tên ${objectName}`}
                                    // defaultValue={['a10', 'c12']}
                                    //   onChange={handleChange} 
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                        {/* danh mục */}
                        <Row>
                            <Col span={24}
                                style={{
                                    // padding:"0 20px",
                                    // margin:"0 -20px"
                                }}>
                                <Form.Item label={<span style={{
                                    fontSize: "16px",
                                    fontWeight: "bold"
                                }}>Danh mục</span>}
                                    name="category"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng chọn tối thiểu 1 danh mục !',
                                        },
                                    ]}
                                    labelCol={{
                                        span: 24,
                                    }}
                                    labelAlign='left'
                                    wrapperCol={{
                                        span: 24,
                                    }}
                                    style={{
                                        display: 'flex',
                                        flexDirection: 'column',
                                    }}>
                                    <Select
                                        mode="multiple"
                                        allowClear
                                        size='large'
                                        placeholder="Chọn danh mục"
                                        // defaultValue={['a10', 'c12']}
                                        //   onChange={handleChange}
                                        style={{
                                            width: '100%',
                                        }}
                                        options={''}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                        {/* thương hiệu và màu sắc */}
                        <Row gutter={20}
                            style={{
                                display: 'flex',
                                // border: '1px solid red',
                            }}>
                            <Col span={12}
                                style={{
                                    // padding:"0 20px",
                                    // margin:"0 -20px"
                                    // border: '1px solid green',
                                }}>
                                <Form.Item label={<span style={{
                                    fontSize: "16px",
                                    fontWeight: "bold"
                                }}>Thương hiệu</span>}
                                    name="brand"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng chọn 1 thương hiệu !',
                                        },
                                    ]}
                                    labelCol={{
                                        span: 24,
                                    }}
                                    labelAlign='left'
                                    wrapperCol={{
                                        span: 24,
                                    }}  >
                                    <Select
                                        placeholder="Chọn thương hiệu"
                                        // defaultValue={['a10', 'c12']}
                                        //   onChange={handleChange}
                                        style={{
                                            width: '100%',
                                        }}
                                        size='large'
                                        options={''}>
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col span={12}
                                style={{
                                    // padding:"0 20px",
                                    // margin:"0 -20px"
                                    // border: '1px solid green',
                                }}>
                                <Form.Item label={<span style={{
                                    fontSize: "16px",
                                    fontWeight: "bold"
                                }}>Màu sắc</span>}
                                    name="color"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng chọn 1 màu sắc !',
                                        },
                                    ]}
                                    labelCol={{
                                        span: 24,
                                    }}
                                    labelAlign="top"
                                    wrapperCol={{
                                        span: 24,
                                    }}
                                    style={{
                                        display: 'flex',
                                        flexDirection: 'column',
                                    }}
                                >
                                    <Select
                                        placeholder="Chọn màu sắc"
                                        // defaultValue={['Chọn màu sắc']}
                                        onChange={() => console.log('hehe')}
                                        style={{
                                            width: '100%',
                                        }}
                                        size='large'
                                        options={''}>
                                    </Select>
                                </Form.Item>
                            </Col>
                
                        </Row>
                        {/* size và số lượng */}
                        <Row gutter={20}
                            style={{
                                display: 'flex',
                                // border: '1px solid red',
                            }}>
                            <Col span={12}
                                style={{
                                    // padding:"0 20px",
                                    // margin:"0 -20px"
                                    // border: '1px solid green',
                                }}>
                                <Form.Item label={<span style={{
                                    fontSize: "16px",
                                    fontWeight: "bold"
                                }}>Size</span>}
                                    name="size"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng chọn 1 size !',
                                        },
                                    ]}
                                    labelCol={{
                                        span: 24,
                                    }}
                                    labelAlign='left'
                                    wrapperCol={{
                                        span: 24,
                                    }}  >
                                    <Select
                                        placeholder="Chọn size"
                                        // defaultValue={['a10', 'c12']}
                                        //   onChange={handleChange}
                                        style={{
                                            width: '100%',
                                        }}
                                        size='large'
                                        options={''}>
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col span={12}
                                style={{
                                    // padding:"0 20px",
                                    // margin:"0 -20px"
                                    // border: '1px solid green',
                                }}>
                                <Form.Item label={<span style={{
                                    fontSize: "16px",
                                    fontWeight: "bold"
                                }}>Số lượng</span>}
                                    name="quantity"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng chọn số lượng !',
                                        },
                                    ]}
                                    labelCol={{
                                        span: 24,
                                    }}
                                    labelAlign="top"
                                    wrapperCol={{
                                        span: 24,
                                    }}
                                    style={{
                                        display: 'flex',
                                        flexDirection: 'column',
                                    }}
                                >
                                    <InputNumber size="large" min={1} max={100000} defaultValue={1} onChange={(value) => console.log("InputNumber changed:", value)}
                                        style={{
                                            width: "100%"
                                        }}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                        {/* giá nhập và giá bán */}
                        <Row gutter={20}
                            style={{
                                // display: 'flex',
                                // border: '1px solid red',
                            }}>
                            <Col span={12}
                                style={{
                                    // padding:"0 20px",
                                    // margin:"0 -20px"
                                    // border: '1px solid green',
                                }}>
                                <Form.Item label={<span style={{
                                    fontSize: "16px",
                                    fontWeight: "bold",
                                    width: '100%',

                                }}>Giá nhập</span>}
                                    name="import"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập giá nhập !',
                                        },
                                    ]}
                                    labelCol={{
                                        span: 24,
                                    }}
                                    labelAlign='left'
                                    wrapperCol={{
                                        span: 24,
                                    }}>

                                    <Input allowClear size='large' placeholder="Nhập giá nhập dạng số "
                                    // defaultValue={['a10', 'c12']}
                                    //   onChange={handleChange} 
                                    />
                                </Form.Item>
                            </Col>
                            <Col span={12}
                                style={{
                                    // padding:"0 20px",
                                    // margin:"0 -20px"
                                    // border: '1px solid green',

                                }}>
                                <Form.Item label={<span style={{
                                    fontSize: "16px",
                                    fontWeight: "bold",

                                }}>Giá bán</span>}
                                    name="export"

                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập giá bán !',
                                        },
                                    ]}
                                    labelCol={{
                                        span: 24,
                                    }}
                                    labelAlign='left'
                                    wrapperCol={{
                                        span: 24,
                                    }}>
                                    <Input allowClear size='large' placeholder="Nhập giá bán dạng số"
                                        // defaultValue={['a10', 'c12']}
                                        //   onChange={handleChange}
                                        style={{
                                            width: "100%",
                                        }}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                        {/* mô tả */}
                        <Row>
                            <Col span={24}>
                                <Form.Item label={<span style={{
                                    fontSize: "16px",
                                    fontWeight: "bold"
                                }}>Mô tả</span>}
                                    name="description"
                                    // rules={[
                                    //   {
                                    //     required: true,
                                    //     message: 'Vui lòng chọn tối thiểu 1 danh mục !',
                                    //   },
                                    // ]}
                                    labelCol={{
                                        span: 24,
                                    }}
                                    labelAlign='left'
                                    wrapperCol={{
                                        span: 24,
                                    }}>
                                    <TextArea rows={4} allowClear maxLength={2000} showCount style={{ resize: 'none', fontSize: "16px" }} placeholder='Nhập mô tả...' />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Col>
                    <Col span={9}
                        style={{
                            padding: "0 20px",
                            // margin:"0 -20px",
                            // border:"1px solid #ccc",
                        }}>
                        <Form.Item
                            label={<span style={{
                                fontSize: "16px",
                                fontWeight: "bold"
                            }}>Ảnh {objectName}</span>}
                            name="description"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng chọn tối thiểu 1 ảnh !',
                                },
                            ]}
                            labelCol={{ span: 24, }}
                            labelAlign='left'
                            wrapperCol={{ span: 24, }}
                            style={{
                                display: 'flex',
                                justifyContent: 'center',
                            }}
                        >
                            <ImgCrop rotationSlider>
                                <Upload
                                    className='custom-upload'
                                    action="https://run.mocky.io/v3/435e224c-44fb-4773-9faf-380c5e6a2188"
                                    listType="picture-card"
                                    fileList={fileList}
                                    onChange={changeImage}
                                    onPreview={previewImage}
                                >
                                    {fileList.length < 1 && '+ Upload'}
                                </Upload>
                            </ImgCrop>
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
        </Modal>
    )
}
