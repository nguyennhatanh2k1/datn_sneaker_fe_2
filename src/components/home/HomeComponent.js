import React from 'react';
import ChartComponent from './ChartComponent'
// import BarChartComponent from './BarChartComponent'
import 'bootstrap/dist/css/bootstrap.min.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBookmark, faClipboardList, faUser, faShoePrints } from '@fortawesome/free-solid-svg-icons';
import { faProductHunt, faShopify } from '@fortawesome/free-brands-svg-icons';
import "../../styles/css/dashboard/theme.css";
import CommonHeader from '../common/Header';
// import CommonFooter from '../common/Footer';

import { Menu, theme, Button, Layout, Space, Table, Tag, Input } from 'antd';
const { Content } = Layout;
const title = "Trang chủ"

export default function HomeComponent({ }) {
    const data = {
        labels: ['Label 1', 'Label 2', 'Label 3', 'Label 4', 'Label 5'],
        datasets: [
            {
                label: 'Sample Bar Chart',
                backgroundColor: 'rgba(75,192,192,1)',
                borderColor: 'rgba(0,0,0,1)',
                borderWidth: 1,
                hoverBackgroundColor: 'rgba(75,192,192,0.4)',
                hoverBorderColor: 'rgba(0,0,0,1)',
                data: [65, 59, 80, 81, 56],
            },
        ],
    };

    const options = {
        // Custom options for the bar chart
        // For example: scales, title, etc.
    };
    return (
        <Layout className="layout"
            style={{
                height: "100%"
            }}>
            <CommonHeader title={title} />
            <Content style={{ padding: "0 40px" }}>
                <h1 style={{ marginBottom: "40px", color: "green", textAlign: "center", fontSize: "40px" }}>Thống kê tháng : 12/2023</h1>
                <div class="row ">
                    <div class="col-lg-12">
                        <div class="row mb-3">
                            <div class="col-xl-4">
                                <section class="card card-featured-left card-featured-primary mb-3">
                                    <div class="card-body">
                                        <div class="widget-summary">
                                            <div class="widget-summary-col widget-summary-col-icon">
                                                <div class="summary-icon bg-primary">
                                                    <FontAwesomeIcon icon={faProductHunt} />                                                </div>
                                            </div>
                                            <div class="widget-summary-col">
                                                <div class="summary">
                                                    <h4 class="title">Doanh thu</h4>
                                                    <div class="info">
                                                        <strong class="amount" id="product-count">2</strong>
                                                    </div>
                                                </div>
                                                {/* <div class="summary-footer">
                                                   <h1>2</h1>
                                                </div> */}
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div class="col-xl-4">
                                <section class="card card-featured-left card-featured-secondary">
                                    <div class="card-body">
                                        <div class="widget-summary">
                                            <div class="widget-summary-col widget-summary-col-icon">
                                                <div class="summary-icon bg-warning">
                                                    <FontAwesomeIcon icon={faBookmark} />                                                </div>
                                            </div>
                                            <div class="widget-summary-col">
                                                <div class="summary">
                                                    <h4 class="title">Tổng số danh mục</h4>
                                                    <div class="info">
                                                        <strong class="amount" id="product-count">2</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div class="col-xl-4">
                                <section class="card card-featured-left card-featured-secondary">
                                    <div class="card-body">
                                        <div class="widget-summary">
                                            <div class="widget-summary-col widget-summary-col-icon">
                                                <div class="summary-icon bg-success">
                                                    <FontAwesomeIcon icon={faShopify} />
                                                    <i class="fas fa-mail-bulk"></i>
                                                </div>
                                            </div>
                                            <div class="widget-summary-col">
                                                <div class="summary">
                                                    <h4 class="title">Tổng số thương hiệu</h4>
                                                    <div class="info">
                                                        <strong class="amount" id="product-count">2</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-xl-4">
                                <section class="card card-featured-left card-featured-tertiary mb-3">
                                    <div class="card-body">
                                        <div class="widget-summary">
                                            <div class="widget-summary-col widget-summary-col-icon">
                                                <div class="summary-icon bg-secondary">
                                                    <FontAwesomeIcon icon={faClipboardList} />
                                                </div>
                                            </div>
                                            <div class="widget-summary-col">
                                                <div class="summary">
                                                    <h5 class="title">Tổng số hóa đơn </h5>
                                                    <div class="info">
                                                        <strong class="amount" id="product-count">2</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div class="col-xl-4">
                                <section class="card card-featured-left card-featured-quaternary">
                                    <div class="card-body">
                                        <div class="widget-summary">
                                            <div class="widget-summary-col widget-summary-col-icon">
                                                <div class="summary-icon bg-info">
                                                    <FontAwesomeIcon icon={faUser} />                                                </div>
                                            </div>
                                            <div class="widget-summary-col">
                                                <div class="summary">
                                                    <h4 class="title">Tổng số nhân viên</h4>
                                                    <div class="info">
                                                        <strong class="amount" id="product-count">2</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div class="col-xl-4">
                                <section class="card card-featured-left card-featured-tertiary mb-3">
                                    <div class="card-body">
                                        <div class="widget-summary">
                                            <div class="widget-summary-col widget-summary-col-icon">
                                                <div class="summary-icon bg-danger">
                                                    <FontAwesomeIcon icon={faShoePrints} />                                                </div>
                                            </div>
                                            <div class="widget-summary-col">
                                                <div class="summary">
                                                    <h4 class="title">Sản phẩm bán chạy</h4>
                                                    <div class="info">
                                                        <strong class="hot-product" id="product-count">Nike Air Force One</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='profit'
                    style={{
                        display: "flex",
                        alignItems: "center",
                        height: "fit-content",
                        // border: "2px solid green"
                    }}>
                    <div className='home-left'
                        style={{
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            height: 'fit-content',
                            width: '50%',
                            flexDirection: 'column',
                            // border: "2px solid yellow"

                        }}>
                        <h1 style={{
                            fontSize: 50,
                            color: "#333"
                        }}>Welcome back !</h1>
                        <p style={{
                            fontSize: 30
                        }}>Ở đây sẽ là thống kê doanh thu và lợi nhuận</p>
                    </div>
                    <div className='home-right'
                        style={{
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            height: '450px',
                            width: '50%',
                            flexDirection: 'column',
                            // border: "2px solid red"
                        }}>
                        <h3
                            style={{
                                marginBottom: "20px",
                                // border: "2px solid blue"
                            }}>Top 5 sản phẩm bán chạy nhất trong tháng</h3>

                        <ChartComponent />
                    </div>

                </div>
            </Content>
            {/* <CommonFooter /> */}
        </Layout>
    );
};