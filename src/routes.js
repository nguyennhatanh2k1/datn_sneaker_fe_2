import React from 'react';
import { Route, Switch, Router } from 'react-router-dom';
import * as page from './pages';
import history from './history'

// import * as pages from './components'


const Routes = () => (
  <div className="content">
    <Router history={history}>  
      <Switch>
        <Route exact path="/" component={page.LoginPage} />
        <Route path="/register" component={page.RegisterPage} />
        <Route path="/home" component={page.HomePage} />
        <Route path="/product" component={page.ProductPage} />
        <Route path="/category" component={page.CategoryPage} />
        <Route path="/brand" component={page.BrandPage} />
        <Route path="/color" component={page.ColorPage} />
        <Route path="/order" component={page.OrderPage} />
        <Route path="/employee" component={page.EmployeePage} />
        <Route path="/account" component={page.AccountPage} />       
      </Switch>
    </Router>
  </div>
);

export default Routes;
