
import Sidebar from '../components/common/sidebar'
// import CircularProgress from '@material-ui/core/CircularProgress';
import ChartComponent from '../components/common/ChartComponent'
import 'bootstrap/dist/css/bootstrap.min.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBookmark,faClipboardList,faUser,faShoePrints} from '@fortawesome/free-solid-svg-icons';
import { faProductHunt, faShopify } from '@fortawesome/free-brands-svg-icons';
import '../styles/dashboard/theme.css';

import React, { useState } from 'react';
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  AudioOutlined
} from '@ant-design/icons';
import {  Menu, theme, Button,Layout,Space, Table, Tag ,Input} from 'antd';
const { Header, Content, Footer } = Layout;
const { Search } = Input;



export default function HomePage() {
  const [collapsed, setCollapsed] = useState(false);

    return (
        <div className="HomePage">
            <Sidebar />
            <div className="main">
                {/* <div>
                    <div class="right-wrapper text-right">
                        <h1>Trang chủ</h1>
                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i
                            class="fas fa-chevron-left"></i></a>
                    </div>
                </div> */}
                <Header
          style={{
            padding: 0,
            // background: colorBgContainer,
            display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          margin:"20px 0"
          }}
        >
          <Button
            type="text"
            icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
            onClick={() => setCollapsed(!collapsed)}
            style={{
              fontSize: '16px',
              width: 64,
              height: 64,
              color:"white"
            }}
          />
          <h1 style={{
        margin: "20px 0",
          color: "#fff"
        }}>Trang chủ</h1>
        <h5 style={{
        margin: "20px 20px 20px 0",
          color: "#fff"
        }}><a>Đăng xuất</a></h5>
        </Header>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row mb-3">
                            <div class="col-xl-4">
                                <section class="card card-featured-left card-featured-primary mb-3">
                                    <div class="card-body">
                                        <div class="widget-summary">
                                            <div class="widget-summary-col widget-summary-col-icon">
                                                <div class="summary-icon bg-primary">
                                                    <FontAwesomeIcon icon={faProductHunt} />                                                </div>
                                            </div>
                                            <div class="widget-summary-col">
                                                <div class="summary">
                                                    <h4 class="title">Số lượng sản phẩm</h4>
                                                    <div class="info">
                                                        <strong class="amount" id="product-count">2</strong>
                                                    </div>
                                                </div>
                                                {/* <div class="summary-footer">
                                                   <h1>2</h1>
                                                </div> */}
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div class="col-xl-4">
                                <section class="card card-featured-left card-featured-secondary">
                                    <div class="card-body">
                                        <div class="widget-summary">
                                            <div class="widget-summary-col widget-summary-col-icon">
                                                <div class="summary-icon bg-warning">
                                                <FontAwesomeIcon icon={faBookmark} />                                                </div>
                                            </div>
                                            <div class="widget-summary-col">
                                                <div class="summary">
                                                    <h4 class="title">Tổng số danh mục</h4>
                                                    <div class="info">
                                                        <strong class="amount" id="product-count">2</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div class="col-xl-4">
                                <section class="card card-featured-left card-featured-secondary">
                                    <div class="card-body">
                                        <div class="widget-summary">
                                            <div class="widget-summary-col widget-summary-col-icon">
                                                <div class="summary-icon bg-success">
                                                    <FontAwesomeIcon icon={faShopify} />
                                                    <i class="fas fa-mail-bulk"></i>
                                                </div>
                                            </div>
                                            <div class="widget-summary-col">
                                                <div class="summary">
                                                    <h4 class="title">Tổng số thương hiệu</h4>
                                                    <div class="info">
                                                        <strong class="amount" id="product-count">2</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-xl-4">
                                <section class="card card-featured-left card-featured-tertiary mb-3">
                                    <div class="card-body">
                                        <div class="widget-summary">
                                            <div class="widget-summary-col widget-summary-col-icon">
                                                <div class="summary-icon bg-secondary">
                                                <FontAwesomeIcon icon={faClipboardList} />
                                                </div>
                                            </div>
                                            <div class="widget-summary-col">
                                                <div class="summary">
                                                    <h4 class="title">Tổng số hóa đơn</h4>
                                                    <div class="info">
                                                        <strong class="amount" id="product-count">2</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div class="col-xl-4">
                                <section class="card card-featured-left card-featured-quaternary">
                                    <div class="card-body">
                                        <div class="widget-summary">
                                            <div class="widget-summary-col widget-summary-col-icon">
                                                <div class="summary-icon bg-info">
                                                <FontAwesomeIcon icon={faUser} />                                                </div>
                                            </div>
                                            <div class="widget-summary-col">
                                                <div class="summary">
                                                    <h4 class="title">Tổng số nhân viên</h4>
                                                    <div class="info">
                                                        <strong class="amount" id="product-count">2</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div class="col-xl-4">
                                <section class="card card-featured-left card-featured-tertiary mb-3">
                                    <div class="card-body">
                                        <div class="widget-summary">
                                            <div class="widget-summary-col widget-summary-col-icon">
                                                <div class="summary-icon bg-danger">
                                                <FontAwesomeIcon icon={faShoePrints} />                                                </div>
                                            </div>
                                            <div class="widget-summary-col">
                                                <div class="summary">
                                                    <h4 class="title">Sản phẩm bán chạy</h4>
                                                    <div class="info">
                                                        <strong class="hot-product" id="product-count">Nike Air Force One</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
                <div style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: '300px',
                    width: '500px',
                    flexDirection: 'column'
                }}>
                    {/* <h1 style={{
                        fontSize: 50
                    }}>Wellcome back !</h1>
                    <CircularProgress />
                    <p style={{
                        fontSize: 30
                    }}>Hãy chọn danh mục khác</p> */}
                    <ChartComponent />
            </div>
            </div></div>)}